## Weather API

A simple weather API web server written in Go. It uses [WeatherDBI](https://weatherdbi.herokuapp.com/) to return the current weather for a given city.

## Cross compilation

```
## Windows
env GOOS=windows GOARCH=amd64 go build -v -o ./bin/weather_api_windows-1.2.exe
## Linux
env GOOS=linux GOARCH=amd64 go build -v -o ./bin/weather_api_linux-1.2
## macos
env GOOS=darwin GOARCH=amd64 go build -v -o ./bin/weather_api_macos-1.2
```

## Usage

The default host for this API is `0.0.0.0` while the default port is port `80`. If you wish to changhe these, set the `HOST` and `PORT` environment variables. For example, to set the host to localhost and port to 8080, use the commands below before running the server:

```bash
export HOST=localhost
export PORT=8080
```


### Using the API

The API has the following endpoints:


##### /weather?city=cityName


Send an HTTP GET Request to the `/weather` endpoint with the required query parameter `city`

```
curl http://localhost:80/weather?city=jeddah

```

##### /health

```bash
curl -i http://localhost:80/health
```

```
{"host":"Khalids-MacBook-Air.local","status":"Healthy"}
```

##### /fatal

```bash
curl -i http://localhost:80/fatal
```

This will kill the server process.