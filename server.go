package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

type CurrentCondition struct {
	DayHour       string `json:"dayhour"`
	Precipitation string `json:"precip"`
	Humidity      string `json:"humidity"`
	Wind          struct {
		Km   int `json:"km"`
		Mile int `json"mile"`
	} `json:"wind"`
	Temperature struct {
		C int `json:"c"`
		F int `json:"f"`
	} `json:"temp"`
}

type Weather struct {
	Region    string           `json:"region"`
	Condition CurrentCondition `json:"currentConditions"`
}

func getHostName() string {
	hostname, err := os.Hostname()
	if err != nil {
		fmt.Println(err)
	}
	return hostname
}

func health(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Methods", "GET")
	w.Header().Set("x-COMING_FROM_HOST", getHostName())
	resp := make(map[string]string)
	resp["status"] = "Healthy"
	resp["host"] = getHostName()
	jsonResp, err := json.Marshal(resp)
	if err != nil {
		log.Fatalf("Error happened in JSON marshal. Err: %s", err)
	}
	fmt.Fprintf(w, string(jsonResp))
}

func fatal(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(http.StatusInternalServerError)
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Methods", "GET")
	w.Header().Set("x-COMING_FROM_HOST", getHostName())
	resp := make(map[string]string)
	resp["status"] = "aborted"
	resp["host"] = getHostName()
	jsonResp, err := json.Marshal(resp)
	if err != nil {
		log.Fatalf("Error happened in JSON marshal. Err: %s", err)
	}
	fmt.Fprintf(w, string(jsonResp))
	log.Fatal("Internal server error")
}

func weather(w http.ResponseWriter, req *http.Request) {
	city := req.URL.Query().Get("city")
	if city == "" {
		w.WriteHeader(http.StatusUnprocessableEntity)
		resp := make(map[string]string)
		resp["message"] = "Missing required parameter: city"
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Methods", "GET")
		w.Header().Set("Access-Control-Allow-Headers",
			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		w.Header().Set("x-COMING_FROM_HOST", getHostName())
		jsonResp, err := json.Marshal(resp)
		if err != nil {
			log.Printf("Error happened in JSON marshal. Err: %s", err)
		}
		w.Write(jsonResp)
		return
	}
	resp, err := http.Get("https://weatherdbi.herokuapp.com/data/weather/" + city)
	if err != nil {
		log.Printf("Failed to send an HTTP Get request. Err: %s", err)
		return
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Failed to read the HTTP response. Err: %s", err)
		return
	}
	var weatherResp Weather
	err = json.Unmarshal(body, &weatherResp)
	if err != nil {
		log.Printf("Failed to parse the HTTP response %s \n. Err: %s", string(body), err)
		return
	}
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Methods", "GET")
	w.Header().Set("Access-Control-Allow-Headers",
		"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	w.Header().Set("x-COMING_FROM_HOST", getHostName())
	jsonResp, _ := json.Marshal(weatherResp)
	w.Write(jsonResp)
}

func main() {
	host := "0.0.0.0"
	port := "80"
	if os.Getenv("HOST") != "" {
		host = os.Getenv("HOST")
	}
	if os.Getenv("PORT") != "" {
		port = os.Getenv("PORT")
	}

	http.HandleFunc("/health", health)
	http.HandleFunc("/weather", weather)
	http.HandleFunc("/fatal", fatal)
	fmt.Println("Listening on http://" + host + ":" + port)
	err := http.ListenAndServe(host+":"+port, nil)
	if err != nil {
		log.Fatalf("Error starting the web server. Err: %s", err)
	}
}
